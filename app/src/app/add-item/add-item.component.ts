import { Component, ElementRef, ViewChild } from '@angular/core';
import { ListService } from '../../service/list.service';
@Component({
  standalone: true,
  selector: 'add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItem {
  @ViewChild('inputName') newItemNameElement! : ElementRef
  @ViewChild('inputPrice') newItemPriceElement! : ElementRef

  constructor(private listService: ListService) {}

  addItem(): void {

    let newItemName = this.newItemNameElement.nativeElement.value
    let newItemPrice = this.newItemPriceElement.nativeElement.value

    console.log("newItem")
    console.log(newItemName, newItemPrice)

    if (newItemName.trim() !== "" && !isNaN(parseFloat(newItemPrice))) {
      const newItem = {
        name: newItemName,
        price: parseFloat(newItemPrice),
        crossed : false
      };
      this.newItemNameElement.nativeElement.value = ""
      this.newItemPriceElement.nativeElement.value = ""

      this.listService.addNewItem(newItem);
    }
  }
}
