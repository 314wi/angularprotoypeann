import { Component, Input } from '@angular/core';
import { ListService } from '../../service/list.service';

@Component({
  standalone: true,
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrl: './item.component.css'
})
export class Item {
  @Input() id: number = 0;
  @Input() itemName: string = "";
  @Input() itemPrice: number = 0;
  @Input() crossedOut = false;

  constructor(private listService: ListService) {}

  handleDelete() {
    console.log("delete");
    this.listService.deleteItem(this.id);
  }

  toggleCrossedOut() {
    this.listService.switchCrossedOut(this.id);
  }
}
