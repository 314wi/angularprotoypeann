import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgFor } from '@angular/common';
import { ListService, ListItem } from '../../service/list.service';

@Component({
  standalone: true,
  selector: 'crossed-list',
  templateUrl: './crossed-list.component.html',
  imports: [NgFor]
})
export class CrossedList implements OnDestroy {
  crossedList: ListItem[] = [];
  private subscription: Subscription;

  constructor(private listService: ListService) {
    this.subscription = this.listService.listObservable.subscribe((map: Map<number, ListItem>) => {
      this.crossedList = Array.from(map.values()).filter(item => item.crossed);
    });
  }


  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
