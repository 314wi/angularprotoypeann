import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgFor } from '@angular/common';
import { Subscription } from 'rxjs';
import { ListService, ListItem } from '../../service/list.service';
import { Item } from '../item/item.component';
import { AddItem } from '../add-item/add-item.component';

@Component({
  standalone: true,
  selector: 'list',
  templateUrl: './list.component.html',
  imports: [Item, AddItem, NgFor],
})
export class List implements OnDestroy {
  shoppingList: [number, ListItem][] = [];
  private subscription: Subscription;

  constructor(private listService: ListService) {
    this.subscription = this.listService.listObservable.subscribe((map: Map<number, ListItem>) => {
      this.shoppingList = Array.from(map.entries());
    });
  }



  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
