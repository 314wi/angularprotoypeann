import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface ListItem {
  id?: number;
  name: string;
  price: number;
  crossed: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ListService {
  private shoppingList: Map<number, ListItem> = new Map([
    [0, { name: "Item 1", price: 10.99, crossed: false }],
    [1, { name: "Item 2", price: 5.49, crossed: false }],
    [2, { name: "Item 3", price: 7.99, crossed: false }]
  ]);

  private listSubject = new BehaviorSubject<Map<number, ListItem>>(this.shoppingList);
  listObservable = this.listSubject.asObservable();

  constructor() { }

  getItem(itemId: number): ListItem | undefined {
    return this.shoppingList.get(itemId);
  }

  addNewItem(newItem: ListItem): void {
    const newId = Math.max(...Array.from(this.shoppingList.keys())) + 1;
    this.shoppingList.set(newId, { id: newId, ...newItem, crossed: false });
    this.listSubject.next(new Map(this.shoppingList));
  }

  deleteItem(itemId: number): void {
    this.shoppingList.delete(itemId);
    this.listSubject.next(new Map(this.shoppingList));
  }

  switchCrossedOut(itemId: number): void {
    const item = this.shoppingList.get(itemId);
    if (item) {
      this.shoppingList.set(itemId, { ...item, crossed: !item.crossed });
      this.listSubject.next(new Map(this.shoppingList));
    }
  }
}
